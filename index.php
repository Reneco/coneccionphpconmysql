<?php
//Damos el nombre de la base de datos, usuario y contraseña, en este caso como no 
//tenemoscontraseña lo dejamos en vacio.
$pdo = new PDO('mysql:host=localhost;dbname=demo', 'root', '');


//Definimos a SQL
$comando = $pdo->prepare("insert into usuario (nombre, email, estado) values(:nombre, :email, :estado)");


echo('exito');
//Llenamos de datos a nuestra trabla creada en nuestra base de datos.
$estado = 'Activo';
$email = 'rrgomez-es@udabol.edu.bo';
$nombre = 'Rene';

$comando->bindParam(':estado', $estado);
$comando->bindParam(':email', $email);
$comando->bindParam(':nombre', $nombre);

//Ejecutamos SQL.
$comando ->execute();


$movil_PDOStatement = $pdo-> prepare("insert into usuario(id,nombre,email,estado) values(?,?,?,?)");

$movil_PDOStatement->bindValue(1,123);
$movil_PDOStatement->bindValue(2,'Rolando');
$movil_PDOStatement->bindValue(3,'pelelopez@hotmail.com');
$movil_PDOStatement->bindValue(4,'estado');    
$movil_PDOStatement->execute();

//Esta linea de codigo sirve para eliminar un usuario de la tabla usuario.
$movil_PDOStatement = $pdo -> prepare("delete from usuario where nombre = ''");
$movil_PDOStatement->execute();



//Esta linea de codigo sirve para actualizar o modificar el nombre de un usuario de la tabla usuarios.
$movil_PDOStatement = $pdo -> prepare("update usuario set nombre = 'Rolando' where nombre='Rene'");
$movil_PDOStatement->execute();

	
?>